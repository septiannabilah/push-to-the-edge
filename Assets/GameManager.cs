﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject NPC;
    public GameObject Torus;
    public GameObject Rapidus;
    public GameObject Magnus;
    public GameObject Enemy;
    public GameObject Loot;
    public GameObject Life;

    public Text Alive;

    public static int[] highscore = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
    public static string[] playerHigh = {
        "not Available",
        "not Available",
        "not Available",
        "not Available",
        "not Available",
        "not Available",
        "not Available",
        "not Available",
        "not Available",
        "not Available",
    };
    public static int EnemyField;
    public static int PlayerHP;
    private int LootItem;
    private int LifeItem;

    public static GameObject Player;

    private int ChosenChar;
    private int ChosenDiff;
    private int enemyTotal;

    public GameObject PanelWin;
    public GameObject pausePanel;
    private bool isPausing;

    public Transform[] randPoints;
    Vector2 spawnPoint;

    public AudioClip WinClip;
    public AudioSource WinSource;

    private void Awake()
    {
        ChosenChar = MainMenu.character;
        Debug.Log(ChosenChar);

        switch (ChosenChar)
        {
            case 1:
                Instantiate(Torus);
                Player = Torus;
                break;
            case 2:
                Instantiate(Rapidus);
                Player = Rapidus;  
                break;
            case 3:
                Instantiate(Magnus);
                Player = Magnus;
                break;
            default:
                Debug.Log("Character Can't Loaded");
                break;
        }

        ChosenDiff = MainMenu.difficulty;

        switch (ChosenDiff)
        {
            case 1:
                enemyTotal = 20;
                PlayerHP = 10;
                LootItem = 14;
                LifeItem = 5;
                break;
            case 2:
                enemyTotal = 30;
                PlayerHP = 8;
                LootItem = 10;
                LifeItem = 4;
                break;
            case 3:
                enemyTotal = 40;
                PlayerHP = 5;
                LootItem = 4;
                LifeItem = 2;
                break;
            default:
                Debug.Log("Enemy can't be Spawned");
                break;
        }

        PanelWin = GameObject.FindGameObjectWithTag("panelWin");

        //for (int i = 1; i < 11; i++)
            //PlayerPrefs.SetInt("HighScoreMed" + i, 50);
    }

    // Start is called before the first frame update
    void Start()
    {
        PanelWin.SetActive(false);

        ChosenDiff = 2;
        enemyTotal = 60;
        EnemyField = enemyTotal + 1;
        AliveCount();


        WinSource.clip = WinClip;

        while (enemyTotal > 0)
        {
            //spawnPoint = new Vector2(Random.Range(randPoints[2].position.x, randPoints[3].position.x), Random.Range(randPoints[0].position.y, randPoints[1].position.y));
            Instantiate(Enemy, new Vector2(Random.Range(randPoints[2].position.x, randPoints[3].position.x), Random.Range(randPoints[0].position.y, randPoints[1].position.y)), Quaternion.identity);
            enemyTotal--;
        }

        while (LootItem > 0)
        {
            Instantiate(Loot, new Vector2(Random.Range(randPoints[2].position.x, randPoints[3].position.x), Random.Range(randPoints[0].position.y, randPoints[1].position.y)), Quaternion.identity);
            LootItem--;
        }

        while (LifeItem > 0)
        {
            Instantiate(Life, new Vector2(Random.Range(randPoints[2].position.x, randPoints[3].position.x), Random.Range(randPoints[0].position.y, randPoints[1].position.y)), Quaternion.identity);
            LifeItem--;
        }

        Time.timeScale = 1;
        isPausing = false;

    }

    private void Update()
    {
        AliveCount();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPausing)
            {
                Time.timeScale = 0;
                pausePanel.SetActive(true);
                isPausing = true;
            } else
            {
                Time.timeScale = 1;
                pausePanel.SetActive(false);
                isPausing = false;
            }
            
        }
    }

    void AliveCount()
    {
        Alive.text = EnemyField.ToString();
        if (EnemyField < 2)
        {
            HighScore();
            Time.timeScale = 0;
            PanelWin.SetActive(true);
            //SoundPlay.SoundSource.Stop();
            WinSource.Play();
        }
    }

    public void HighScore()
    {
        switch (ChosenDiff)
        {
            case 1:
                PlayerPrefs.SetInt("HighScoreEz", EnemyField);
                PlayerPrefs.SetString("PlayerHighScoreEz", Player.ToString());
                Debug.Log("char : " + PlayerPrefs.GetString("PlayerHighScoreEz"));
                Debug.Log("alive : " + PlayerPrefs.GetInt("HighScoreEz", 0));
                break;
            case 2:
                PlayerPrefs.SetInt("HighScoreMed", EnemyField);
                PlayerPrefs.SetString("PlayerHighScoreMed", Player.ToString());
                Debug.Log("char : " + PlayerPrefs.GetString("PlayerHighScoreMed"));
                Debug.Log("alive : " + PlayerPrefs.GetInt("HighScoreMed", 0));
                break;
            case 3:
                PlayerPrefs.SetInt("HighScoreHard", EnemyField);
                PlayerPrefs.SetString("PlayerHighScoreMed", Player.ToString());
                Debug.Log("char : " + PlayerPrefs.GetString("PlayerHighScoreMed"));
                Debug.Log("alive : " + PlayerPrefs.GetInt("HighScoreMed", 0));
                break;
        }
    }

}
