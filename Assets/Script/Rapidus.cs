﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rapidus : PlayerMovement
{
    private float dashSpeed = 20f;
    private float skillTimer;
    public float startSkillTimer = 3f;
    private float dashTime;
    public float startTimeDash = 5f;
    private int direction = 0; 

    // Update is called once per frame
    void Update()
    {
        //Turning();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rapidusSkill();
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            HeartBeatSource.Play();
            returnNormal();
        }
    }

    void rapidusSkill()
    {
        if (skill > 0)
        {
            HeartBeatSource.Stop();
            SoundSource.Play();
            speed = dashSpeed;
            dashTime -= Time.deltaTime;
            skill -= 1;
            SkillPoints();
            Debug.Log("dash started : " + skill);
        }
    }

    void returnNormal()
    {
        speed = 6;
        dashTime = startTimeDash;
        skillTimer = startSkillTimer;
        Debug.Log("dash stopped");
        
    }
}
