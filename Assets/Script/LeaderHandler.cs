﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LeaderHandler : MonoBehaviour
{
    public GameObject[] txtScore;
    public GameObject[] txtName;

    private List<Leader> leaderList;
    private Text[] score;
    private Text[] name;
    private int total;

    void Start()
    {
        leaderList = new List<Leader>();
        for (int i = 0; i < 10; i++)
        {
            score[i] = txtScore[i].GetComponent<Text>();
            score[i].text = "";
            name[i] = txtName[i].GetComponent<Text>();
            name[i].text = "";
        }
        
        prepare();
        fillData();
    }

    void prepare()
    {
        total = PlayerPrefs.GetInt("total");
        total = total >= 5 ? 5 : total;

        if (total != null)
        {
            for (int i = 0; i < total; i++)
            {
                string name = PlayerPrefs.GetString("name_" + i);
                int level = PlayerPrefs.GetInt("level_" + i);

                leaderList.Add(new Leader(name, level));
            }

            leaderList = leaderList.OrderByDescending(x => x.getLevel())
                .ToList();
        }
    }

    void fillData()
    {
        for (int i = 0; i < leaderList.Count; i++)
        {
            Leader leader = leaderList[i];

            score[i].text = leader.getLevel().ToString();
            name[i].text = leader.getName().ToString();
        }
    }
}
