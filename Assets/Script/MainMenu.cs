﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public static int difficulty = 2;
    public static int character = 2;

    public GameObject playPanel;
    public GameObject LeadPanel;
    public GameObject creditPanel;
    public GameObject SettingsPanel;
    public GameObject ExitPanel;

    public Text[] charPlayer;
    public Text[] alivePlayer;
    public GameObject chooseChar;

    public AudioClip SoundClip;
    public AudioSource SoundSource;

    private void Start()
    {
        SoundSource.clip = SoundClip;
        //charPlayer.text = PlayerPrefs.GetString("HighScoreEz", "Unavailable");
        //alivePlayer.text = "#" + PlayerPrefs.GetInt("HighScoreEz", 0).ToString();

    }

    public void playButton()
    {
        SoundSource.Play();
        creditPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        LeadPanel.SetActive(false);
        playPanel.SetActive(true);
    }

    public void LeadButton()
    {
        SoundSource.Play();
        playPanel.SetActive(false);
        creditPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        LeadPanel.SetActive(false);
        LeadPanel.SetActive(true);
    }

    public void creditButton()
    {
        SoundSource.Play();
        playPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        LeadPanel.SetActive(false);
        creditPanel.SetActive(true);
    }

    public void SettingsButton()
    {
        SoundSource.Play();
        playPanel.SetActive(false);
        creditPanel.SetActive(false);
        LeadPanel.SetActive(false);
        SettingsPanel.SetActive(true);
    }

    public void ExitBtn()
    {
        SoundSource.Play();
        playPanel.SetActive(false);
        creditPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        LeadPanel.SetActive(false);
        ExitPanel.SetActive(true);
    }

    public void backMainMenu()
    {
        SoundSource.Play();
        Application.LoadLevel("MainMenu");
    }

    public void easyDiff()
    {
        SoundSource.Play();
        difficulty = 1;
        chooseChar.SetActive(true);
    }

    public void mediumDiff()
    {
        SoundSource.Play();
        difficulty = 2;
        chooseChar.SetActive(true);
    }

    public void hardDiff()
    {
        SoundSource.Play();
        difficulty = 3;
        chooseChar.SetActive(true);
    }

    public void torusChar()
    {
        SoundSource.Play();
        character = 1;
        Application.LoadLevel("SampleScene");
    }

    public void rapidusChar()
    {
        SoundSource.Play();
        character = 2;
        Application.LoadLevel("SampleScene");
    }

    public void magnusChar()
    {
        SoundSource.Play();
        character = 3;
        Application.LoadLevel("SampleScene");
    }

    public void NoBtn()
    {
        //playPanel.SetActive(false);
        //creditPanel.SetActive(false);
        SoundSource.Play();
        ExitPanel.SetActive(false);
    }

    public void YesBtn()
    {
        SoundSource.Play();
        Application.Quit();
        Debug.Log("Quit Game");
    }

    public void Difficulty(int diff)
    {
        switch (diff)
        {
            case 0:
                for (int i = 0; i < 10; i++)
                {
                    charPlayer[i].text = PlayerPrefs.GetString("PlayerHighScoreEz" + 1);
                    alivePlayer[i].text = "#" + PlayerPrefs.GetInt("HighScoreEz" + 1, 0).ToString();
                }
                break;
            case 1:
                for (int i = 0; i < 10; i++)
                {
                    charPlayer[i].text = PlayerPrefs.GetString("PlayerHighScoreMed" + 1);
                    alivePlayer[i].text = "#" + PlayerPrefs.GetInt("HighScoreMed" + 1, 0).ToString();
                }
                break;
            case 2:
                for (int i = 0; i < 10; i++)
                {
                    charPlayer[i].text = PlayerPrefs.GetString("PlayerHighScoreHard" + 1);
                    alivePlayer[i].text = "#" + PlayerPrefs.GetInt("HighScoreHard" + 1, 0).ToString();
                }
                break;
        }
    }

}
