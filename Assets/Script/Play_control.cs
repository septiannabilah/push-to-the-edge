﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Play_control : MonoBehaviour {

    public GameObject player;
    public float speed;
    public float rotSpeed;

    private void Update()
    {
       if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            movingUp();
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            movingDown();
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            movingLeft();
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            movingRight();
        }
        
    }

    private void LateUpdate()
    {
        var mouse = Input.mousePosition;
        var screenPoint = Camera.main.WorldToScreenPoint(transform.localPosition);
        var offset = new Vector2(mouse.x - screenPoint.x, mouse.y - screenPoint.y);
        var angle = Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }

    public void movingUp()
    {
        Vector2 newPosition = (Vector2)this.transform.position + Vector2.up * speed * Time.deltaTime;
        this.transform.position = newPosition;
    }
    
    public void movingDown()
    {
        Vector2 newPosition = (Vector2)this.transform.position - Vector2.up * speed * Time.deltaTime;
        this.transform.position = newPosition;
    }

    public void movingLeft()
    {
        Vector2 newPosition = (Vector2)this.transform.position - Vector2.right * speed * Time.deltaTime;
        this.transform.position = newPosition;
    }

    public void movingRight()
    {
        Vector2 newPosition = (Vector2)this.transform.position + Vector2.right * speed * Time.deltaTime;
        this.transform.position = newPosition;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "circle")
        {
            Destroy(this.gameObject);
        }
    }
}
