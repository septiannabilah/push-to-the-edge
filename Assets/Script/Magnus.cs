﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnus : PlayerMovement
{
    // Rigidbody2D rigidbody2D;
    private float MagnusMass = 20859f;

    private void Update()
    {
        //Turning();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MagnusSkill();
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            HeartBeatSource.Play();
            // rigidbody2D.mass = 0.0001f;
            playerRigidbody.mass = 0.0001f;
            speed = 6;
        }

        Debug.Log(playerRigidbody.mass);
    }

    void MagnusSkill()
    {
        if (skill > 0)
        {
            HeartBeatSource.Stop();
            SoundSource.Play();
            // rigidbody2D.mass = torusMass;
            playerRigidbody.mass = MagnusMass;
            speed = 0;
        }
        
    }
}
