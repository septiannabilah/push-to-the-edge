﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform target;
    private GameObject player;
    public float smoothing = 5f;

    Vector3 offset;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        target = player.transform;
        offset = transform.position - target.position;
    }

    private void FixedUpdate()
    {
        Vector3 targetCamPos = target.position + offset;

        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);

    }


}
