﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{
    public float speed;
    private float waitTime;
    public float startWaitTime = 10;

    public float disTarget = 5f; // distance size from the target
    public Transform moveSpot;
    Vector2 spotTarget;

    public Transform[] spotPos;
    public Transform patrolSpot;
    private Transform target;

    public AudioClip SoundClip;
    public AudioSource SoundSource;


    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

        waitTime = startWaitTime;

        spotTarget = new Vector2(Random.Range(spotPos[2].position.x, spotPos[3].position.x), 
            Random.Range(spotPos[0].position.y, spotPos[1].position.y));

        SoundSource.clip = SoundClip;

        // Instantiate(patrolSpot, moveSpot);

        Collider2D PC = this.GetComponent("Collider2D") as Collider2D;

    }

    private void Update()
    {
        
        if (Vector2.Distance(transform.position, target.position) < disTarget)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, 
                speed * Time.deltaTime);
            SoundSource.Play();
            Debug.Log("Chase");
        }
        else if (Vector2.Distance(transform.position, target.position) > disTarget)
        {
            transform.position = Vector2.MoveTowards(transform.position, 
                spotTarget, speed * Time.deltaTime);
            SoundSource.Stop();
            Debug.Log("patrol");

            if (waitTime <= 0)
            {
                spotTarget = new Vector2(Random.Range(spotPos[2].position.x, spotPos[3].position.x), 
                    Random.Range(spotPos[0].position.y, spotPos[1].position.y));
                waitTime = startWaitTime;
            }
            else if (transform.position == moveSpot.position)
            {
                spotTarget = new Vector2(Random.Range(spotPos[2].position.x, spotPos[3].position.x), 
                    Random.Range(spotPos[0].position.y, spotPos[1].position.y));
                waitTime = startWaitTime;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }

        Debug.Log(target);

        if (target.transform == null)
        {
            SoundSource.Stop();
            Debug.Log("enemy sound Stop");
        }

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "circle")
        {
            Destroy(this.gameObject);
            GameManager.EnemyField -= 1;
        }

    }
}
