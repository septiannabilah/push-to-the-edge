﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehaviour : MonoBehaviour
{

    public AudioClip SoundClip;
    public AudioSource SoundSource;

    // Start is called before the first frame update
    void Start()
    {
        SoundSource.clip = SoundClip;
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "circle")
        {
            SoundSource.Play();
            Destroy(this.gameObject);
        }
    }
}
