﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    protected int healthPoint;
    protected int skill;

    public Vector3 movement;
    public Rigidbody2D playerRigidbody;
    public GameObject cameraDummy;
    int floorMask;

    public Text hPoint;
    public Text sPoint;
    public GameObject PanelLost;
    public Text Number;

    public AudioClip SoundClip;
    public AudioSource SoundSource;
    public AudioClip ChewingClip;
    public AudioSource ChewingSource;
    public AudioClip HealthClip;
    public AudioSource HealthSource;
    public AudioClip SkillClip;
    public AudioSource SkillSource;
    public AudioClip LoseClip;
    public AudioSource LoseSource;
    public AudioClip HeartBeatClip;
    public AudioSource HeartBeatSource;

    GameManager gameManager;

    protected void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");

        playerRigidbody = GetComponent<Rigidbody2D>();

        hPoint = GameObject.FindGameObjectWithTag("hp").GetComponent<Text>();

        sPoint = GameObject.FindGameObjectWithTag("sp").GetComponent<Text>();

        PanelLost = GameObject.FindGameObjectWithTag("panelLose");

        LoseSource = GameObject.FindGameObjectWithTag("WinLoseBGM").GetComponent<AudioSource>();
    }

    void Start()
    {
        healthPoint = GameManager.PlayerHP;
        LifeHP();
        Debug.Log("Player HP : " + healthPoint);

        skill = 3;
        SkillPoints();
        Debug.Log("Player Skill : " + skill);

        Time.timeScale = 1;
        PanelLost.SetActive(false);

        SoundSource.clip = SoundClip;
        ChewingSource.clip = ChewingClip;
        HealthSource.clip = HealthClip;
        SkillSource.clip = SkillClip;
        LoseSource.clip = LoseClip;
        HeartBeatSource.clip = HeartBeatClip;

        HeartBeatSource.Play();

    }

    protected void FixedUpdate()
    {

        // menggerakkan player
        Move();

        // arah pandangan player 
        Turning();
    }

    public void Move()
    {
        // menyimpan axis raw input dari user
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        // mengatur gerak berdasarkan input axis 
        movement.Set(horizontal, vertical, 0f);

        // mengembalikan movement vector 
        movement = movement.normalized * speed * Time.deltaTime;

        // memindahkan player ke posisi sesuai input 
        playerRigidbody.MovePosition(transform.position + movement);
    }

    public void Turning()
    {
        var mouse = Input.mousePosition;
        var screenPoint = Camera.main.WorldToScreenPoint(transform.localPosition);
        var offset = new Vector3(mouse.x - screenPoint.x, mouse.y - screenPoint.y, mouse.z - screenPoint.z);
        var angle = Mathf.Atan2(offset.x, offset.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, -angle);

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "circle")
        {
            Destroy(this.gameObject);
            GameOver();
        }

        if (collision.gameObject.tag == "Loot")
        {
            SkillSource.Play();
            skill += 1;
            SkillPoints();
            Debug.Log("skill add : " + skill);
        }

        if (collision.gameObject.tag == "Life")
        {
            HealthSource.Play();
            GameManager.PlayerHP += 1;
            LifeHP();
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            //healthPoint--;
            GameManager.PlayerHP--;
            LifeHP();
            ChewingSource.Play();
            Debug.Log("Collision with enemy : " + GameManager.PlayerHP);
        }
        
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            ChewingSource.Stop();
        }
    }

    void LifeHP()
    {
        hPoint.text = GameManager.PlayerHP.ToString();
        if (GameManager.PlayerHP < 0)
        {
            //Destroy(this.gameObject);
            GameOver();
            Debug.Log("Game Over SHIT");
        }
    }

    protected void SkillPoints()
    {
        sPoint.text = skill.ToString();
    }

    void GameOver()
    {
        Time.timeScale = 0;
        
        //SoundPlay.SoundSource.Stop();
        ChewingSource.Stop();
        HeartBeatSource.Stop();
        LoseSource.volume = 0.2f;
        LoseSource.Play();
        PanelLost.SetActive(true);
        Number = GameObject.FindGameObjectWithTag("number").GetComponent<Text>();
        Number.text = "#" + GameManager.EnemyField.ToString();
        HighScore();
        //Destroy(this.gameObject);
    }

    public void HighScore()
    {
        

        switch (MainMenu.difficulty)
        {
            case 1:           
                for (int i = 0; i < 11; i++)
                {
                    GameManager.highscore[i] = PlayerPrefs.GetInt("HighScoreEz" + i + 1, 0);
                    Debug.Log("HighScoreEz" + i + 1 + " : " + GameManager.highscore[i]);
                }
                for (int i = 0; i < 10; i++)
                {
                    GameManager.playerHigh[i] = PlayerPrefs.GetString("PlayerHighScoreEz" + i + 1);
                }
                for (int i = 1; i < 11; i++)
                {
                    if (GameManager.EnemyField < PlayerPrefs.GetInt("HighScoreEz" + i, 0))
                    {
                        PlayerPrefs.SetInt("HighScoreEz" + i, GameManager.EnemyField);
                        int k = i;
                        while (k < 11)
                        {
                            PlayerPrefs.SetInt("HighScoreEz" + k + 1, GameManager.highscore[k]);
                            PlayerPrefs.SetString("PlayerHighScoreMed" + k + 1, GameManager.playerHigh[k]);
                            k++;
                        }
                        Debug.Log("Char : " + PlayerPrefs.GetString("HighScoreEz" + i));
                        Debug.Log("HighScoreEz" + i + " : " + PlayerPrefs.GetInt("HighScoreEz" + i, 0));
                        break;
                    }
                    else
                    {
                        
                        continue;
                    }
                    
                }
                break;

            case 2: 
                for (int i = 0; i < 10; i++)
                {
                    GameManager.highscore[i] = PlayerPrefs.GetInt("HighScoreMed" + i + 1, 0);
                    Debug.Log("HighScoreEz" + i + 1 + " : " + GameManager.highscore[i]);
                    //GameManager.highscore[i] = i + 50;
                }
                for (int i = 0; i < 10; i++)
                {
                    GameManager.playerHigh[i] = PlayerPrefs.GetString("PlayerHighScoreMed" + i + 1);
                }
                for (int i = 1; i < 11; i++)
                {
                    if (GameManager.EnemyField < PlayerPrefs.GetInt("HighScoreMed" + i, 0))
                    {
                        PlayerPrefs.SetString("PlayerHighScoreMed" + i, GameManager.Player.ToString());
                        PlayerPrefs.SetInt("HighScoreMed" + i, GameManager.EnemyField);
                        int k = i - 1;
                        while (k < 10)
                        {
                            PlayerPrefs.SetInt("HighScoreMed" + k + 1, GameManager.highscore[k]);
                            PlayerPrefs.SetString("PlayerHighScoreMed" + k + 1, GameManager.playerHigh[k]);
                            k++;
                        }
                        Debug.Log("Char : " + PlayerPrefs.GetString("HighScoreMed" + i));
                        Debug.Log("HighScoreMed" + i + " : " + PlayerPrefs.GetInt("HighScoreMed" + i, 0));
                        break;
                    }
                    else
                    {
                        
                        continue;
                    }

                }
                break;

            case 3:
                for (int i = 0; i < 11; i++)
                {
                    GameManager.highscore[i] = PlayerPrefs.GetInt("HighScoreHard" + i + 1, 0);
                }
                for (int i = 0; i < 10; i++)
                {
                    GameManager.playerHigh[i] = PlayerPrefs.GetString("PlayerHighScoreEz" + i + 1);
                }
                for (int i = 1; i < 11; i++)
                {
                    if (GameManager.EnemyField < PlayerPrefs.GetInt("HighScoreHard" + i, 0))
                    {
                        
                        PlayerPrefs.SetInt("HighScoreHard" + i, GameManager.EnemyField);
                        int k = i;
                        while (k < 11)
                        {
                            PlayerPrefs.SetInt("HighScoreHard" + k + 1, GameManager.highscore[k]);
                            PlayerPrefs.SetString("PlayerHighScoreHard" + k + 1, GameManager.playerHigh[k]);
                            k++;
                        }
                        Debug.Log("Char : " + PlayerPrefs.GetString("HighScoreHard" + i));
                        Debug.Log("HighScoreHard" + i + " : " + PlayerPrefs.GetInt("HighScoreHard" + i, 0));
                        break;
                    }
                    else
                    {
                        
                        continue;
                    }

                }
                break;
        }
    }

}
