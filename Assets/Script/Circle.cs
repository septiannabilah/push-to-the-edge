﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Circle : MonoBehaviour
{

    public float shortingSize = 0.5f;
    public float countDown = 0;
    public float circleTime = 16f;
    public float startRestrict = 7f;
    private float restrictTime = 0f;
    private int circleCount = 8;
    public int circleLimit = 16;
    private Transform circle;

    public Text RestrictTime;

    // Start is called before the first frame update
    void Start()
    {
        // circle = GameObject.FindGameObjectWithTag("Circle").GetComponent<Transform>();
        countDown = circleTime;

        RestrictCount();
    }

    // Update is called once per frame
    void Update()
    {
        if (countDown < 0 && circleCount > 0)
        {
            circleShorting();
            circleCount -= 1;
            countDown = circleTime;
        }
        else
        {
            countDown -= Time.deltaTime;
        }

        RestrictCount();

    }

    public void circleShorting()
    {
        transform.localScale -= new Vector3(shortingSize, shortingSize, 0);
        circleTime -= 1f;
    }

    void RestrictCount()
    {
        string minSec = string.Format("{0}:{1:00}", (int)countDown / 60, (int)countDown % 60);
        RestrictTime.text = minSec;
    }
}
