﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torus : PlayerMovement 
{

    // Rigidbody2D rigidbody2D;
    private float torusMass = 8f;

    private void Update()
    {
        Turning();
        if (Input.GetKeyDown(KeyCode.Space)){
            TorusSkill();
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            HeartBeatSource.Play();
            // rigidbody2D.mass = 0.0001f;
            playerRigidbody.mass = 0.0001f;
        }
    }

    void TorusSkill()
    {
        if (skill > 0)
        {
            HeartBeatSource.Stop();
            SoundSource.Play();
            // rigidbody2D.mass = torusMass;
            playerRigidbody.mass = torusMass;
        }
        
    }
}
